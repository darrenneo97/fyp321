//Read me Consumer server


Required: Kafka, Python, MySQL

pip3 install kafka_python
pip3 install Flask
---------------------------------------
to start kafka server:
use command prompt
go to the file location kafka
type:
bin/zookeeper-server-start.sh config/zookeeper.properties
--------------in a seperate cmd -------------------- 
bin/kafka-server-start.sh config/server.properties
-----------------------------------------------------------
To setup MySQL:
mysql -u bill -p
passpass

To create db:
create database ksql;

To use db:
use ksql;
----------------------------
To create table:
CREATE TABLE `kafka_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(10) UNIQUE,
  `message` varchar(3000) UNIQUE,
  `date` datetime NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;
-------------------------------------------------------------------

To start server type: 
python3 consumer_server22dec.py
