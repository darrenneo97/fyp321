from flask import Flask, render_template, url_for, request, json, redirect
from kafka import KafkaConsumer

from json import loads
import json
import sys
from flaskext.mysql import MySQL

import pymysql


mysql = MySQL()
                         
app = Flask(__name__)
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/', methods=['GET', 'POST'])
def index():
  return render_template('monitorApp.html')

#Test kafka
consumer2 = KafkaConsumer('cart_topic',
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 

#msg = consumer2.value
#for i in consumer2:
  #print(i.value)
                         
#consumer_product = KafkaConsumer('product', bootstrap_server=['localhost:9092'])

#producer = KafkaProducer(bootstrap_servers=['10.0.2.15:9092'], value_serializer=json_serializer)
def consumedata():
    #Consume from kafka
  topic = request.form['topic_name']
  
  consumer = KafkaConsumer(topic,
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 
  for i in consumer:
    print(i.value)
    return i.value
  return i.value


@app.route('/senddata', methods =['POST'])
def senddata():
  
  #Connect to mysql 
  con = mysql.connect()
  cursor = con.cursor()
  _data = consumedata()
  #send data
  cusor.callproc('sendtopic',(_data))
  print(_data)
  cursor.close()
  con.close()
  con.commit()
  


def getData():
  con = mysql.connect()
  cursor = con.cursor()
  cursor.callproc('getdata', (_topic,))
  data = cursor.fetchall()
  print(data)
  cursor.close()
  con.close()

if __name__ == '__main__':
  app.run(port=5002, debug=True)


    
#for message in consumer:
    #with open("output.txt", "a") as myfile:
  	 # myfile.write(message.value)
  	 


