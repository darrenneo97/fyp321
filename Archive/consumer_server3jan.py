from flask import Flask, render_template, url_for, request, json, redirect
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file




#mysql -u bill -p
#passpass

#create database ksql

#CREATE TABLE `kafka_topic` (
#  `id` int(11) NOT NULL AUTO_INCREMENT,
#  `message` varchar(5000) DEFAULT NULL,
#  `date` datetime DEFAULT NULL,
#  `topic` varchar(50) NOT NULL,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=1;

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#To get current time in string
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-6]
  return str_now


#Home page  
#Check server status
@app.route('/', methods=['GET', 'POST'])
def index():
  consumer = kafka.KafkaConsumer(bootstrap_servers=['localhost:9092'])
  topics = consumer.topics()
  #Check kafka consumer status
  print(topics)
  if not topics:
    server_status ="Not running"
  else:
    server_status = "Running"
  
  root_url = request.url_root

  #print(root_url)
  #Check Python URL 
  developer_url = 'http://127.0.0.1:5002/'
  if root_url != developer_url:
    e_server = "Not running"
  else:
    e_server = "Running"
  
  #Connect to mysql
  con = mysql.connect()
  cursor = con.cursor()
  
  #Total registered user and total user online function
  #sql statement
  user_login = "user_logon"
  reg = "user_creation"
  user_logout = "user_logout"
  cursor.execute("""SELECT COUNT(*) from topic_data WHERE topic= %s""", (reg,))
  
  #Get total users registered
  _total_user = cursor.fetchone()
  total_user = _total_user[0]
  

  #Get total user online -> total user login - total user logout
  cursor.execute("""SELECT COUNT(*) from topic_data WHERE topic= %s""", (user_login,))
  total_login = cursor.fetchone()
  _total_login = total_login[0]

  cursor.execute("""SELECT COUNT(*) from topic_data WHERE topic= %s""", (user_logout,))
  total_logout = cursor.fetchone()
  _total_logout = total_logout[0]
  
  total_online= (_total_login - _total_logout)
  
  #Get top search function
  search= "search_topic"
  cursor.execute("""SELECT * from topic_data WHERE topic= %s""", (search,))
  
  data = cursor.fetchall()
  if len(data) != 0:
    search_dict = []
    temp_dict = {}
    #calculate each of the key words
    for i in data:
      if i[2] not in temp_dict:
        temp_dict[i[2]] = 0
    
      temp_dict[i[2]] = temp_dict[i[2]] + 1
      #search_dict.append(temp_dict)
  
    #print(search_dict)
    print(temp_dict)
   
    _max_key = max(temp_dict, key=temp_dict.get)
    max_key = _max_key[17:]
  
    all_values = temp_dict.values()
    max_value = max(all_values)
    con.commit()
    con.close()
    cursor.close()
    return render_template('monitorApp.html', e_server =e_server, server_status =server_status, total_user = total_user, total_online=total_online, max_key  = max_key, max_value=max_value)
    
  else:
    return render_template('monitorApp.html', e_server =e_server, server_status =server_status, total_user = total_user, total_online=total_online)
    
    

    


#Consume from kafka then send it to mySQL
#After that, retrive it from MYSQL to display it on HTML
@app.route('/getdata', methods =['GET'])
def getdata():
  #Topic name = request from html
  topic = request.form['topic_name']
  session['topic'] = topic
  global counter 
  
  #Initialize consumer
  consumer = KafkaConsumer(topic,
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     consumer_timeout_ms=1000,
     value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  
  #initialize mysql
  con = mysql.connect()
  cursor = con.cursor()
  
  #Break down consumer in lines
  for i in consumer:
    message = i.value
    _message = str(message)
    msg_dict = {"activity": message["activity"], "time" : message["time"], "email": message["email"]}
    print("Test : " + _message) 
      
    time = timenow() 
    #insert into database
    cursor.execute("INSERT IGNORE INTO kafka_topic(message, date, topic) VALUES (%s, %s, %s)", (_message, time, topic))
    
    #Insert into database (another table)
    cursor.execute("INSERT IGNORE INTO topic_data(email, activity, message, date, topic) VALUES (%s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], _message, msg_dict["time"], topic))
    counter +=1
  
  con.commit()
  con.close()  
  success = "Consume success"
  #End of first method
  
  #Initialize mySQL
  con = mysql.connect()
  cursor = con.cursor()
  #get topic from session
  topic = session.get('topic')
  
  #Select statement SQL
  cursor.execute("""SELECT * FROM topic_data WHERE  topic= %s""", (topic,))
  #Fetch all data
  data = cursor.fetchall()
  #data_1 = cursor.fetchone()
  #print(data) #For debug purpose
  
  #Open file store in file to be downloaded
  f = open('%s.txt' % topic, "w")
  
  #To sort the mysql DATA
  data_dict = []
  for i in data:
  #Temp dict for easy sorting of data from SQL
    temp_dict = {
      "ID" : i[0],
      "Email" : i[1],
      "Activity": i[2],
      "Message" : i[3],
      "Date" : i[4],
      "Topic": i[5]}
    #Output to file 
    f.write('ID: ')
    f.write(str(i[0]))
    f.write(', Topic: ')
    f.write(str(i[5]))
    f.write("\nEmail: " )
    f.write(i[1])
    f.write('Date: ')
    f.write( str(i[4]))
    f.write('\nEvent Occured: ')
    f.write(str(i[2]))
    f.write("\nfull Message: ")
    f.write(i[3])
    f.write('\n')
    f.write('\n')
    #temp dict gets appended to data_dict
    data_dict.append(temp_dict)
    
  f.close()
  cursor.close()
  con.close()
  #Return the dict to display it on HTML
  return render_template("displaytopic.html", data_dict = data_dict)
  
  
#Download button function    
@app.route('/download')
def downloadFile():
  #get topic input
  topic = session.get('topic')
  
  #string variable
  path = "%s.txt" %topic
  f = path
  return send_file(f, as_attachment=True)
  
#total user function  
@app.route('/totalusers')
def totaluser():
  con = mysql.connect()
  cursor = con.cursor()
  query = "SELECT COUNT(*) from topic_data where topic = `user_logon`" 
  cursor.execute(query)
  _total_user = cursor.fetchone()
  total_user = _total_user[0]
  print(total_user)
  return "xxx"

#Counter
counter = 0
def uniqueid(self):
  self.counter+= 1

if __name__ == '__main__':
  app.run(port=5002, debug=True)


    

  	 


