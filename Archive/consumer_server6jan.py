from flask import Flask, render_template, url_for, request, json, redirect, jsonify
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file
from flask import flash
import random


#mysql -u bill -p
#passpass

#create database ksql

#REFER TO MYSQLREADME.TXT FOR MORE INFO!

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#To get current time in string
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-6]
  return str_now


#home page
@app.route('/', methods=['GET'])
def index():
  topic = "server"
  consumer = KafkaConsumer(topic, bootstrap_servers=['localhost:9092'],auto_offset_reset='earliest',consumer_timeout_ms=1000, value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  

  topics = consumer.topics()
  #Check kafka consumer status
  print(topics)
  if not topics:
    server_status ="Not running"
  else:
    server_status = "Running"
  
  

  
  #Check Python URL 
  e_dict = {}
  for i in consumer:
    message = i.value
    print(message)
    e_dict = {"status": message["status"]}
  e_server= e_dict.get('status')
  print(e_server)
  return render_template('monitorApp.html',  e_server =e_server, server_status =server_status)



@app.route('/total_reg', methods=['GET'])    
def total_reg():
  con = mysql.connect()
  cursor = con.cursor()
  reg = "Registration"
  sqlstatement = "SELECT COUNT(*) from account_service WHERE activity= %s"
  
  cursor.execute(sqlstatement, reg)
  #Get total users registered
  _total_user = cursor.fetchone()
  total_user = _total_user[0]
  
  con.close()
  cursor.close()
  #con.commit()
  return jsonify(total_user=total_user)

#Consume from kafka then send it to mySQL
#After that, retrive it from MYSQL to display it on HTML
@app.route('/index/monitorlog', methods=['POST'])
def monitorlog():
  #Topic name = request from html
  topic = request.form['topic_name']
  session['topic'] = topic
  

  #get topic from session
  topic = session.get('topic')
  
  try:
    
    #Initialize mySQL
    con = mysql.connect()
    cursor = con.cursor() 
    
    if topic == "user_logon":
      svc = "account_service"
      activity = "Login"
      
    if topic == "user_logout":
      svc = "account_service"
      activity = "Logout"
      
    if topic == "user_creation":
      svc = "account_service"
      activity = "Registration"
      
    if topic == "cart_service":
      svc = "cart_service"
      activity = "Added to cart"
      
    if topic == "search_service":
      svc = "search_service"
      activity = "Searched" 
    if topic == "order_service":
      svc = "order_service"
      activity = "Ordered" 

    session['svc'] = svc
    session['activity'] = activity
    #Select statement SQL
    table_var_str = svc
    
    st ='select * FROM ' + table_var_str + ' where activity = ' + '"'+ activity +'"'
    #print(st)
    cursor.execute(st)

    
    record = cursor.fetchall()
    #Pass records to identify topic function
    data_dict = identifyTopic(record)
    
    cursor.close()
    con.close()
    
    #Return the data_dict to display it on HTML
    if topic == "user_logon":
      return render_template("monitorlogin.html", data_dict = data_dict) 

    if topic == "user_logout":
      return render_template("monitorlogout.html", data_dict = data_dict) 
  
      
    if topic == "user_creation":
      return render_template("monitorsignup.html", data_dict = data_dict) 

      
    if topic == "cart_service":
      return render_template("monitorcart.html", data_dict = data_dict) 

      
    if topic == "search_service":
      return render_template("monitorsearch.html", data_dict = data_dict) 

    if topic == "order_service":
      return render_template("displaytopic.html", data_dict = data_dict) 
    
    
  except:
    flash("Data cannot be empty!")
    return redirect('/')

@app.route('/cart_rate')    
def cart_rate():
  topic = session.get('topic')
  con = mysql.connect()
  cursor = con.cursor() 
  val = "Added to cart"
  sqlstatement = "SELECT * from cart_service WHERE activity= %s"
  
  cursor.execute(sqlstatement, val)
  data = cursor.fetchall()
  print(data)
  
  cursor.close()
  con.close()
  data_list=[]
  data_dict={}
  cart_list = []
  #for i in data:
    #if data[3] not in data_dict:
      #data_dict[i] = 0
    #data_dict[i]  = data_dict[i] + 1
     
  for i in data:  
      
    temp_dict = {"cartitem": i[3]}
    data_list.append(temp_dict)
    
  for i in data_list:
    #print(i)
    j = i["cartitem"]
    #Check if i exist in data_dict 
    if j not in data_dict:
      #initiate value for dictionary item : 0
      data_dict[j] = 0
    #item : 0 + 1  
    data_dict[j] = data_dict[j] + 1
  f = open('%s.txt' % topic, "w")  
  for i in data_dict:
    temp_dict = {"Item_name"  : i,
                 "Cart_rate" :data_dict.get(i)}
    f.write("Item_name: ")
    f.write(i)
    f.write("\nCart_rate: ")
    
    f.write(str(data_dict.get(i)))
    f.write("\n")
     
    cart_list.append(temp_dict)
  print(cart_list)
  f.close()

  
  #print("t: " + str(letter_counts['t']) + " occurrences")  
  
    
  return render_template('cartrate.html', cart_list=cart_list)

#Download button function    
@app.route('/download')
def downloadFile():
  #get topic input
  topic = session.get('topic')
  
  #string variable
  path = "%s.txt" %topic
  f = path
  return send_file(f, as_attachment=True)
  
#total user function  
@app.route('/user_online', methods=['GET'])    
def user_online():
  con = mysql.connect()
  cursor = con.cursor()
  login = "Login"
  logout = "Logout"
  #Get login number
  sqlstatement = "SELECT COUNT(*) from account_service WHERE activity= %s"
  
  cursor.execute(sqlstatement, login)
  _logins = cursor.fetchone()
  logins = _logins[0]
  
  #Get logout number
  cursor.execute(sqlstatement, logout)
  _logouts = cursor.fetchone()
  logouts = _logouts[0]
  
  online = (int(logins) - int(logouts))
  
  print(online)
  
  
  return jsonify(online = online)
@app.route('/topsearch', methods=['GET'])    
def topsearch():
  con = mysql.connect()
  cursor = con.cursor()
  val = "search_service"
  sqlstatement = "SELECT * from search_service WHERE topic= %s"
  cursor.execute(sqlstatement, val)
  data = cursor.fetchall()
  #print(data)
  try:
    if len(data) != 0:
      search_dict = []
      temp_dict = {}
      #calculate each of the key words
      for i in data:
        #print(i)
        
        if i[3] not in temp_dict:
          temp_dict[i[3]] = 0
      
        temp_dict[i[3]] = temp_dict[i[3]] + 1
        #search_dict.append(temp_dict)
    
      #print(search_dict)
      print(temp_dict)
     
      _max_key = max(temp_dict, key=temp_dict.get)
      max_key = _max_key
    
      all_values = temp_dict.values()
      max_value = max(all_values)
      con.commit()
      con.close()
      cursor.close()
    return jsonify(max_key = max_key, max_value=max_value)
  except:
    return "Error: There is no search data" 

@app.route('/filter', methods=['POST'])
def filter():

  con = mysql.connect()
  cursor = con.cursor()
  userinput = request.form['filter'] 
  #select = request.form['selectfilter'] 
  topic = session.get('topic')
  #data = session.get('data')

  activity = session.get('activity')
  svc = session.get('svc')

  sql = "select * from " + svc + " where activity = %s AND email = %s"

  val = (activity, userinput)
  cursor.execute(sql, val)
  
  record = cursor.fetchall() 
  print("TEST:")
  print(record)
  
  
  cursor.close()
  con.close()
  data_dict = []
  
  data_dict = identifyTopic(record)
  
  return render_template('displaytopic.html', data_dict=data_dict, filtertext=userinput)

@app.route('/identifyTopic', methods=['POST'])
def identifyTopic(record):
  
  topic = session.get('topic')
  data_dict = []
  f = open('%s.txt' % topic, "w")
  if topic =="cart_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "item" : i[3],
        "qty" : i[4],
        "Message" : i[5],
        "Date" : i[6],
        "Time": i[7],
        "Topic": i[8]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[7]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write('\nDate: ')
      f.write( str(i[6]))
      f.write('\nitem_name: ')
      f.write(str(i[3]))
      f.write(', Quantity: ')
      f.write(str(i[4]))
      #f.write("\nfull Message: ")
      #f.write(i[5])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)

  if topic =="search_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "keyword" : i[3],
        "Date" : i[5],
        "Time": i[6],
        "Topic": i[7]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[5]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('Date: ')
      f.write( str(i[5]))
      f.write('Time: ')
      f.write( str(i[6]))
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write("\nkeyword: ")
      f.write(i[3])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)
  if topic == "user_logon" or topic == "user_logout" or topic ==  "user_creation":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "Message" : i[3],
        "Date" : i[4],
        "Time" : i[5],
        "Topic": i[6]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[5]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('Date: ')
      f.write( str(i[4]))
      f.write('Time: ')
      f.write( str(i[5]))
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write("\nfull Message: ")
      f.write(i[3])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)     
  if topic == "order_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "orderID" : i[3],
        "Date" : i[5],
        "Topic": i[7],
        "sliceID": i[4][50:64]
        }
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[5]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('Date: ')
      f.write( str(i[5]))
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write("\nOrder ID: ")
      f.write(i[3])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)      
  return (data_dict)     

  

  
#Counter
counter = 0
def uniqueid(self):
  self.counter+= 1
#total_reg()
if __name__ == '__main__':
  app.run(port=5002, debug=True)


    

  	 


