from flask import Flask, render_template, url_for, request, json, redirect
from kafka import KafkaConsumer

from json import loads
import json
import sys
from flaskext.mysql import MySQL

import pymysql


mysql = MySQL()
                         
app = Flask(__name__)
app.secret_key = 'why would I tell you my secret key?'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/', methods=['GET', 'POST'])
def index():
  return render_template('monitorApp.html')

consumer2 = KafkaConsumer('user_creation',
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 
                         
#consumer_product = KafkaConsumer('product', bootstrap_server=['localhost:9092'])

#producer = KafkaProducer(bootstrap_servers=['10.0.2.15:9092'], value_serializer=json_serializer)

def kafka_consume():
  topic = _request.form['topic_name']
  consumer = KafkaConsumer(topic,
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 
  return consumer.value

def senddata():
  con = mysql.connect()
  cursor = con.cursor()
  _data = kafka_consumer


def consume():
  con = mysql.connect()
  cursor = con.cursor()
  cursor.callproc('getdata', (_topic,))
  data = cursor.fetchall()
  print(data)


print("1. User_logon\n")
print("2. User_creation\n")
print("3. Cart_topic\n")
userinput = input("Enter your topic of choice: ")
if userinput == 1:
  topic= 'user_logon'
  consumer = KafkaConsumer(topic,
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 
  for message in consumer:
    # message value and key are raw bytes -- decode if necessary!
    # e.g., for unicode: `message.value.decode('utf-8')`
    print(message.value)
    text= str(message.value)
    f = open("user_logon.txt", "a")
	
    f.write(text)
    f.close()
    sys.exit()
elif userinput == 2:
  topic= 'user_creation'
  consumer = KafkaConsumer(topic,
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         bootstrap_servers=['localhost:9092']) 
  for message in consumer:
    # message value and key are raw bytes -- decode if necessary!
    # e.g., for unicode: `message.value.decode('utf-8')`
    print(message.value)
    text= str(message.value)
    f = open("user_creation.txt", "a")
	
    f.write(text)
    f.close()
    sys.exit()
elif userinput == 3:
  topic= 'cart_topic'
  #consumer = KafkaConsumer(topic,
                         #group_id='my-group',
                         #consumer_timeout_ms=1000, 
                         #bootstrap_servers=['localhost:9092']) 
  consumer = KafkaConsumer ('cart_topic',bootstrap_servers = ['localhost:9092'], value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  for message in consumer:
    # message value and key are raw bytes -- decode if necessary!
    # e.g., for unicode: `message.value.decode('utf-8')`
    print(message.value)
    text= str(message.value)
    f = open("cart_topic.txt", "a")
	
    f.write(text+ "\n")
    f.close()
    #sys.exit()
else: 
  print("you have entered an invalid choice")
  
  
sys.exit()


    
#for message in consumer:
    #with open("output.txt", "a") as myfile:
  	 # myfile.write(message.value)
  	 


