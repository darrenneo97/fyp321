from flask import Flask, render_template, jsonify, request
import random
from flaskext.mysql import MySQL
app = Flask(__name__)
mysql = MySQL()
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route("/")
def test_job():
    return render_template('monitorApp.html')
  
@app.route('/stuff', methods=['GET'])
def stuff():
  con = mysql.connect()
  cursor = con.cursor()
  reg = "Registration"
  cursor.execute("""SELECT COUNT(*) from account_service WHERE activity= %s""", (reg,))

  #Get total users registered
  _total_user = cursor.fetchone()
  total_user = _total_user[0]
  con.commit()
  con.close()
  cursor.close()
  #print(total_user)
  #return jsonify(total_user = total_user)
  val_random = random.random()   
  print(total_user)
  return jsonify(total_user=total_user)
  
if __name__ == "__main__":
    app.run(debug=True)
