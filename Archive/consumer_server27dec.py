from flask import Flask, render_template, url_for, request, json, redirect
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file




#mysql -u bill -p
#passpass

#create database ksql

#CREATE TABLE `kafka_topic` (
#  `id` int(11) NOT NULL AUTO_INCREMENT,
#  `message` varchar(5000) DEFAULT NULL,
#  `date` datetime DEFAULT NULL,
#  `topic` varchar(50) NOT NULL,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=1;

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#To get current time in string
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-6]
  return str_now





#Home page  
#Check server status
@app.route('/', methods=['GET', 'POST'])
def index():
  consumer = kafka.KafkaConsumer(bootstrap_servers=['localhost:9092'])
  topics = consumer.topics()
  #Check kafka consumer status
  print(topics)
  if not topics:
    server_status ="Not running"
  else:
    server_status = "Running"
  
  root_url = request.url_root

  #print(root_url)
  #Check Python URL 
  developer_url = 'http://127.0.0.1:5002/'
  if root_url != developer_url:
    e_server = "Not running"
  else:
    e_server = "Running"

  return render_template('monitorApp.html', e_server =e_server, server_status =server_status)
    
    


#Consume from kafka then send it to mySQL
#After that, retrive it from MYSQL to display it on HTML
@app.route('/senddata', methods =['POST'])
def senddata():
  #Topic name = request from html
  topic = request.form['topic_name']
  session['topic'] = topic
  global counter 
  
  
  #Initialize consumer
  consumer = KafkaConsumer(topic,
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     consumer_timeout_ms=1000,
     value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  
  #initialize mysql
  con = mysql.connect()
  cursor = con.cursor()
  
  #Break down consumer in lines
  for i in consumer:
    message = i.value
    _message = str(message)
    msg_dict = {"activity": message["activity"], "time" : message["time"], "user": message["user"]}
    print("Test : " + _message) 
      
    time = timenow() 
    #insert into kafka
    cursor.execute("INSERT IGNORE INTO kafka_topic(message, date, topic) VALUES (%s, %s, %s)", (_message, time, topic))
    
    cursor.execute("INSERT IGNORE INTO topic_data(email, activity, message, date, topic) VALUES (%s, %s, %s)", (msg_dict["email"], msg_dict["activity"], _message, msg_dict["time"], topic))
    counter +=1
  
  con.commit()
  con.close()  
  success = "Consume success"
  #End of first method
  
  #Initialize mySQL
  con = mysql.connect()
  cursor = con.cursor()
  #get topic from session
  topic = session.get('topic')
  
  #Select statement SQL
  cursor.execute("""SELECT * FROM kafka_topic WHERE  topic= %s""", (topic,))
  #Fetch all data
  data = cursor.fetchall()
  #data_1 = cursor.fetchone()
  #print(data) #For debug purpose
  
  #Open file store in file to be downloaded
  f = open('%s.txt' % topic, "w")
  
  #To sort the mysql DATA
  data_dict = []
  for i in data:
  #Temp dict for easy sorting of data from SQL
    temp_dict = {
      "ID" : i[0],
      "Message" : i[1],
      "Date" : i[2],
      "Topic": i[3]}
    #Output to file 
    f.write('ID: ')
    f.write(str(i[0]))
    f.write(', Topic: ')
    f.write(str(i[3]))
    f.write('\nDate: ')
    f.write( str(i[2]))
    f.write('\nActivity: ')
    f.write(str(i[1]))
    f.write('\n')
    f.write('\n')
    #temp dict gets appended to data_dict
    data_dict.append(temp_dict)
    
  f.close()
  cursor.close()
  con.close()
  #Return the dict to display it on HTML
  return render_template("displaytopic.html", data_dict = data_dict)
  
  
#Download button function    
@app.route('/download')
def downloadFile():
  #
  topic = session.get('topic')
  path = "%s.txt" %topic
  f = path
  return send_file(f, as_attachment=True)

counter = 0
def uniqueid(self):
  self.counter+= 1

if __name__ == '__main__':
  app.run(port=5002, debug=True)


    

  	 


