from flask import Flask, render_template, url_for, request, json, redirect
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file




#mysql -u bill -p
#passpass

#create database ksql

#CREATE TABLE `kafka_topic` (
#  `id` int(11) NOT NULL AUTO_INCREMENT,
#  `message` varchar(5000) DEFAULT NULL,
#  `date` datetime DEFAULT NULL,
#  `topic` varchar(50) NOT NULL,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=1;

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


def pull_data():
  #Topic name = request from html
  #topic = request.form['topic_name']
  #session['topic'] = topic
  
  topic = "kafka"
  #Initialize consumer
  consumer = KafkaConsumer(topic,
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='latest',
     enable_auto_commit=True,
     consumer_timeout_ms=3000,
     value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  
  #initialize mysql
  con = mysql.connect()
  cursor = con.cursor()
 

  #Break down consumer in linesS
  for i in consumer:
    message = i.value
    _message = str(message)
    msg_dict = {"activity": message["activity"], "time" : message["time"][:-1], "email": message["email"], "topic" : message["topic"]}
    
    
    print("Activity detected : " + _message) 
    
    if message["topic"] == "account_service":
      cursor.execute("INSERT IGNORE INTO account_service(email, activity, message, date, time, topic) VALUES (%s, %s, %s, %s, %s,%s)", (msg_dict["email"], msg_dict["activity"], _message, msg_dict["time"][:10], msg_dict["time"][11:], message["topic"]))
    if message["topic"] == "cart_service":
      cursor.execute("INSERT IGNORE INTO cart_service(email, activity, item_name, quantity, message, date,time, topic) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], message["item_name"], message["quantity"], _message, msg_dict["time"][:10], msg_dict["time"][11:],message["topic"]))
    if message["topic"] == "search_service":
      cursor.execute("INSERT IGNORE INTO search_service(email, activity, keyword, message, date,time, topic) VALUES (%s, %s, %s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], message["keyword"], _message, msg_dict["time"][:10], msg_dict["time"][11:], message["topic"]))
    if message["topic"] == "order_service":
      cursor.execute("INSERT IGNORE INTO order_service(email, activity, orderid, message, date,time, topic) VALUES (%s, %s, %s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], message["orderid"], _message, msg_dict["time"][:10], msg_dict["time"][11:], message["topic"]))
      
      
    
    #insert into database (Without dictionary list)
    #cursor.execute("INSERT IGNORE INTO kafka_topic(message, date, topic) VALUES (%s, %s, %s)", (_message, time, topic))
    

  con.commit()
  con.close()  
 


while True:  
  pull_data()
