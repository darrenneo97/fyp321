//mysql readme


#sudo mysql –u root –p

CREATE USER 'bill' IDENTIFIED BY 'passpass';

Create database ksql;
Create database ACCOUNTS;

GRANT ALL PRIVILEGES ON *.* TO 'bill'@'ksql';
GRANT ALL PRIVILEGES ON *.* TO 'bill'@'ACCOUNTS';

SHOW GRANTS FOR bill;

To change user to bill:
exit
mysql -u bill -p
passpass

--------------------
use ACCOUNTS;
----------------------
For ACCOUNTS database:

Create table
`category` varchar(70) DEFAULT NULL,
--------------
CREATE TABLE `products` (
  `pid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(70) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `price` int(10) DEFAULT NULL,
  `discount` int(10) DEFAULT NULL
 );
 
CREATE TABLE `tbl_user` (
  `user_id` BIGINT UNIQUE AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `user_email` VARCHAR(45) UNIQUE,
  `user_password` TEXT(200) NULL,
  PRIMARY KEY (`user_email`)
  );

CREATE TABLE cart (
    cartID int NOT NULL AUTO_INCREMENT,
    item_name varchar(200),
    item_code varchar(200) NOT NULL,
    quantity int NOT NULL,
    unit_price int NOT NULL,
    user_email varchar(200),
    PRIMARY KEY (cartID), 
    FOREIGN KEY (user_email) REFERENCES tbl_user(user_email)
);




  
  
  
CREATE TABLE `tbl_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL,
  `mobile_number` varchar(45) DEFAULT NULL,
  `message` varchar(5000) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;


CREATE TABLE tbl_order (
    orderid int NOT NULL AUTO_INCREMENT,
    total_price int NOT NULL,
    user_email varchar(200),
    date datetime DEFAULT NULL,
    PRIMARY KEY (orderid)    
);
CREATE TABLE order_details (
    orderdetailid int NOT NULL AUTO_INCREMENT,
    orderid int NOT NULL,
    item_name varchar(200),
    item_code varchar(200) NOT NULL,
    quantity int NOT NULL,
    unit_price int NOT NULL,
    user_email varchar(200),
    PRIMARY KEY (orderdetailid),
    FOREIGN KEY (orderid) REFERENCES tbl_order(orderid)    
);

--------------------------------------
Insert data into products
-----------------------------


INSERT INTO `products` (`pid`, `code`, `name`, `image`, `price`, `discount`) VALUES
(1,'s1001', 'Aluminium Foil', '1001.jpg', 5, 1),
(2,'s1002','Apple','1002.jpg',4,0),
(3,'s1003','Bacon','1003.jpg',3,0),
(4,'s1004','Baking Powder','1004.jpg',2,0),
(5,'s1005','Baking Soda','1005.jpg',2,0),
(6,'s1006','Banana','1006.jpg',2,0),
(7,'s1007','Beer','1007.jpg',2,0),
(8,'s1008','Blue Berries','1008.jpg',2,0),
(9,'s1009','Cabbage','1009.jpg',2,0),
(10,'s1010','Carrot','1010.jpg',2,0),
(11,'s1011','Cheese','1011.jpg',2,0),
(12,'s1012','Cherry','1012.jpg',2,0),
(13,'s1013','Chicken Eggs','1013.jpg',2,0),
(14,'s1014','Chocolate','1014.jpg',2,0),
(15,'s1015','Fish Cake','1015.jpg',2,0),
(16,'s1016','Flour','1016.jpg',2,0),
(17,'s1017','Grape','1017.jpg',2,0),
(18,'s1018','Green Tea','1018.jpg',2,0),
(19,'s1019','Guava','1019.jpg',2,0),
(20,'s1020','Ham','1020.jpg',2,0),
(21,'s1021','Jasmine Tea','1021.jpg',2,0),
(22,'s1022','Kiwi','1022.jpg',2,0),
(23,'s1023','Mango','1023.jpg',2,0),
(24,'s1024','Meat Ball','1024.jpg',2,0),
(25,'s1025','Milk','1025.jpg',2,0),
(26,'s1026','Mineral Water','1026.jpg',2,0),
(27,'s1027','Mushroom','1027.jpg',2,0),
(28,'s1028','Nuggets','1028.jpg',2,0),
(29,'s1029','Orange','1029.jpg',2,0),
(30,'s1030','Pear','1030.jpg',2,0),
(31,'s1031','Pepsi','1031.jpg',2,0),
(32,'s1032','Pineapple','1032.jpg',2,0),
(33,'s1033','Raspberries','1033.jpg',2,0),
(34,'s1034','Red Wine','1034.jpg',2,0),
(35,'s1035','Root Beer','1035.jpg',2,0),
(36,'s1036','Sausage','1036.jpg',5,0),
(37,'s1037','strawberries','1037.jpg',2,0),
(38,'s1038','Watermelon','1038.jpg',2,0),
(39,'s1039','White Wine','1039.jpg',5,0),
(40,'s1040','Winter Melon','1040.jpg',5,0);
--------------------------------------
mysql procedures
-------------------------------------
DELIMITER $$
CREATE DEFINER=`bill`@`localhost` PROCEDURE `sp_createUser`(
    IN p_name VARCHAR(20),
    IN p_email VARCHAR(20),
    IN p_password TEXT(200)
)
BEGIN
    if ( select exists (select 1 from tbl_user where user_email = p_email) ) THEN
     
        select 'Username Exists !!';
     
    ELSE
     
        insert into tbl_user
        (
            name,
            user_email,
            user_password
        )
        values
        (
            p_name,
            p_email,
            p_password
        );
     
    END IF;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`bill`@`localhost` PROCEDURE `sp_validateLogin`(
IN p_email VARCHAR(20)
)
BEGIN
    select * from tbl_user where user_email=p_email ;
END$$
DELIMITER ;




----------------------------------------------------------------------------------
USE `ACCOUNTS`;
DROP procedure IF EXISTS `ACCOUNTS`.`feedback`;
 
DELIMITER $$
USE `ACCOUNTS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `feedback`(
    IN p_name varchar(45),
    IN p_user_email varchar(50),
    IN p_mobile_number varchar(50),
    IN p_message varchar(1000)
    
)
BEGIN
    insert into tbl_feedback(
        name,
        user_email,
        mobile_number,
        message,  
        date
    )
    values
    (
        p_name,
        p_user_email,
        p_mobile_number,
        p_message,
        NOW()
    );
END$$
 
DELIMITER ;

------------------------------------
USE `ACCOUNTS`;
DROP procedure IF EXISTS `getfeedback`;
 
DELIMITER $$
USE `ACCOUNTS`$$
CREATE PROCEDURE `getfeedback` (
IN p_user_email varchar(50)
)
BEGIN
    select * from tbl_feedback where user_email = p_user_email;
END$$
 
DELIMITER ;
-----------------------------------

---------------------------------------

USE `ksql`;
DELIMITER $$
USE `ksql`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sendtopic`(

    IN p_message varchar(1000)
    
)
BEGIN
    insert into kafka_topic(
        message,  
        date
    )
    values
    (
        
        p_message,
        NOW()
    );
END$$
 
DELIMITER ;
---------------------------------
#Ideal table: with a seperated email and "date" column
for easy filter.
---------------------------------
#This is for consumer server database.
use ksql;


CREATE TABLE `kafka_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(3000) UNIQUE,
  `date` datetime NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `topic_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `message` varchar(3000) UNIQUE,
  `date` datetime NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

#-----------------------------------
CREATE TABLE `account_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `message` varchar(3000) UNIQUE,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `cart_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `quantity` int(10) NOT NULL,
  `message` varchar(3000) UNIQUE,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `search_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `message` varchar(3000) UNIQUE,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `topic` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `order_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `orderid` varchar(100) NOT NULL,
  `message` varchar(3000) UNIQUE,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `topic` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `content` varchar(3000) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(100) NOT NULL,
  `checked_status` boolean DEFAULT False,
  `message` varchar(3000) UNIQUE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


CREATE TABLE `user` (
  `id` BIGINT UNIQUE AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `password` TEXT(200) NOT NULL,
  `type` TEXT(50) NOT NULL,
  `delete_log` boolean DEFAULT False,
  `create_admin` boolean DEFAULT False,
  `create_user` boolean DEFAULT False,
  `view_log` boolean DEFAULT True,
  PRIMARY KEY (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


#Insert a super user into data base
INSERT INTO user (id, user_name, password, type, delete_log, create_admin, create_user, view_log) VALUES
(1, 'superuser', 123, 'admin', True, True, True, True);

INSERT INTO user (user_name, password, type) VALUES
('normaluser', 123, 'user');
---------------------------------------------

#Test data
INSERT INTO account_service (id, email, activity, message, date, time, topic) VALUES 
(1000, '15jan@gmail.com', 'Login', 'test message', '2022-02-01', '19:09:46', 'account_service');


select * from account_service where activity = "Login" AND date BETWEEN '2022-01-01' AND '2022-01-31';

select * from account_service where activity = "Login" AND date BETWEEN '2022-02-01' AND '2022-02-31';



